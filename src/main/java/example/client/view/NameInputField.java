package example.client.view;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.Event;
import example.client.ExampleAppResources;

public class NameInputField {

    private static final String INPUT_PLACEHOLDER_LABEL = "Enter your name";
    private static final String DEFAULT_UNHIGHLIGHT_COLOR = "#eff3f6";
    private static final String DEFAULT_HIGHTLIGHT_COLOR = "#00AFD6";
    private static final String DEFAULT_INVALID_COLOR = "#FF2E3A";

    private final Document document = Document.get();
    private BodyElement body = document.getBody();

    private Element parent;

    private DivElement container = document.createDivElement();
    private ImageElement iconView = document.createImageElement();
    private InputElement nameInput = document.createTextInputElement();

    public NameInputField() {
        container.appendChild(iconView);
        container.appendChild(nameInput);
        container.setAttribute("style", "display: flex; align-items: center;");
        container.addClassName(ExampleAppResources.INSTANCE.css().name_field());

        container.getStyle().setWidth(220, Style.Unit.PX);
        container.getStyle().setHeight(34, Style.Unit.PX);
        container.getStyle().setBorderWidth(1, Style.Unit.PX);

        iconView.setSrc(ExampleAppResources.INSTANCE.loginIconImage().getSafeUri().asString());
        iconView.setWidth(16);
        iconView.setHeight(16);

        iconView.getStyle().setMargin(8, Style.Unit.PX);

        nameInput.setAttribute("style", "min-width: 80%;");
        nameInput.getStyle().setBorderWidth(0, Style.Unit.PX);
        nameInput.setPropertyString("placeholder", INPUT_PLACEHOLDER_LABEL);

        setFocusHandlers();
    }

    public NameInputField(double width, Style.Unit widthUnits, double height, Style.Unit heightUnits) {

        container.appendChild(iconView);
        container.appendChild(nameInput);
        container.setAttribute("style", "display: flex; align-items: center;");
        container.addClassName(ExampleAppResources.INSTANCE.css().name_field());

        container.getStyle().setWidth(width, widthUnits);
        container.getStyle().setHeight(height, heightUnits);
        container.getStyle().setBorderWidth(1, Style.Unit.PX);

        iconView.setSrc(ExampleAppResources.INSTANCE.loginIconImage().getSafeUri().asString());
        iconView.setWidth(16);
        iconView.setHeight(16);

        iconView.getStyle().setMargin(8, Style.Unit.PX);

        nameInput.setAttribute("style", "min-width: 80%;");
        nameInput.getStyle().setBorderWidth(0, Style.Unit.PX);
        nameInput.setPropertyString("placeholder", INPUT_PLACEHOLDER_LABEL);

        setFocusHandlers();
    }

    private void setFocusHandlers() {
        Event.sinkEvents(nameInput, Event.ONCLICK | Event.ONBLUR);
        Event.setEventListener(nameInput, event -> {
            if (Event.ONCLICK == event.getTypeInt()) {
                container.getStyle().setBorderColor(DEFAULT_HIGHTLIGHT_COLOR);
            } else if (Event.ONBLUR == event.getTypeInt()) {
                container.getStyle().setBorderColor(DEFAULT_UNHIGHLIGHT_COLOR);
            }
        });
    }

    public void attachTo(final Element parent) {
        this.parent = parent;
        if (parent != null) {
            parent.appendChild(container);
        }
    }

    public void detach() {
        if (document != null) {
            if (body.isOrHasChild(container)) {
                container.removeAllChildren();
                container.removeFromParent();
            }
        }
    }

    public String getNameValue() {
        return nameInput.getValue();
    }

    public Element get() {
        return container;
    }

    public void highlight(boolean isValid) {
        if (isValid) {
            container.getStyle().setBorderColor(DEFAULT_UNHIGHLIGHT_COLOR);
        } else {
            container.getStyle().setBorderColor(DEFAULT_INVALID_COLOR);
        }
    }
}
