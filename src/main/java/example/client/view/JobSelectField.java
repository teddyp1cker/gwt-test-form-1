package example.client.view;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.Event;
import example.client.ExampleAppResources;

import java.util.List;

public class JobSelectField {

    private static final String DEFAULT_UNHIGHLIGHT_COLOR = "#eff3f6";
    private static final String DEFAULT_HIGHTLIGHT_COLOR = "#00AFD6";
    private static final String DEFAULT_INVALID_COLOR = "#FF2E3A";

    private final Document document = Document.get();
    private BodyElement body = document.getBody();

    private Element parent;

    private DivElement container = document.createDivElement();
    private ImageElement iconView = document.createImageElement();

    public SelectElement jobSelector = document.createSelectElement();

    public JobSelectField() {
        createElementsStructure();

        container.getStyle().setWidth(220, Style.Unit.PX);
        container.getStyle().setHeight(34, Style.Unit.PX);
        container.getStyle().setBorderWidth(1, Style.Unit.PX);

        iconView.setSrc(ExampleAppResources.INSTANCE.passwordIconImage().getSafeUri().asString());
        iconView.setWidth(16);
        iconView.setHeight(16);
        iconView.getStyle().setMargin(8, Style.Unit.PX);

        jobSelector.setAttribute("style", "min-width: 80%;");
    }

    private void createElementsStructure() {
        container.appendChild(iconView);
        container.appendChild(jobSelector);
        container.setAttribute("style", "display: flex; align-items: center;");
        container.addClassName(ExampleAppResources.INSTANCE.css().job_choose_field());
        enableFocusEventPropagation();
    }

    private void enableFocusEventPropagation() {
        container.setAttribute("tabindex", "1");
    }

    public JobSelectField(double width, Style.Unit widthUnits, double height, Style.Unit heightUnits) {
        createElementsStructure();

        container.getStyle().setWidth(width, widthUnits);
        container.getStyle().setHeight(height, heightUnits);
        container.getStyle().setBorderWidth(1, Style.Unit.PX);

        iconView.setSrc(ExampleAppResources.INSTANCE.passwordIconImage().getSafeUri().asString());
        iconView.setWidth(16);
        iconView.setHeight(16);
        iconView.getStyle().setMargin(8, Style.Unit.PX);

        jobSelector.setAttribute("style", "min-width: 80%;");

        setFocusHandlers();
    }

    private void setFocusHandlers() {
        Event.sinkEvents(container, Event.ONCLICK | Event.ONBLUR);
        Event.setEventListener(container, event -> {
            if (Event.ONCLICK == event.getTypeInt()) {
                container.getStyle().setBorderColor(DEFAULT_HIGHTLIGHT_COLOR);
            } else if (Event.ONBLUR == event.getTypeInt()) {
                container.getStyle().setBorderColor(DEFAULT_UNHIGHLIGHT_COLOR);
            }
        });
    }

    public int getJobSelectedIndex() {
        return jobSelector.getSelectedIndex();
    }

    public void attachTo(final Element parent) {
        this.parent = parent;
        if (parent != null) {
            parent.appendChild(container);
        }
    }

    public void detach() {
        if (document != null) {
            if (body.isOrHasChild(container)) {
                container.removeAllChildren();
                container.removeFromParent();
            }
        }
    }

    public void addJobs(final List<String> jobsNames) {
        for (String jobsName : jobsNames) {
            OptionElement jobOption = document.createOptionElement();
            jobOption.setText(jobsName);
            jobSelector.add(jobOption, null);
        }

        jobSelector.setSelectedIndex(-1);
    }

    public Element get() {
        return container;
    }

    public void highlight(boolean isValid) {
        if (isValid) {
            container.getStyle().setBorderColor(DEFAULT_UNHIGHLIGHT_COLOR);
        } else {
            container.getStyle().setBorderColor(DEFAULT_INVALID_COLOR);
        }
    }
}
