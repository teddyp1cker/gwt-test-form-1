package example.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.Event;
import example.client.model.JobsProvider;
import example.client.view.JobSelectField;
import example.client.view.NameInputField;

import java.util.ArrayList;
import java.util.List;

public class ExampleEntryPoint implements EntryPoint {

    private static final String NAME_HEADER_LABEL = "Name";
    private static final String JOB_HEADER_LABEL = "Job";
    private static final String SEND_BUTTON_LABEL = "Send";
    private static final String PLEASE_FILL_NAME_FIELD_LABEL = "Please fill name field";
    private static final String PLEASE_ACCEPT_AGR_LABEL = "Please check the checkbox";
    private static final String PLEASE_CHOOSE_JOB_LABEL = "Please choose job";
    private static final String AGREE_TO_SEND_INFORMATION_LABEL = "I agree to send information";
    private static final int FORM_SECTIONS_COUNT = 4;

    private final Document document = Document.get();
    private BodyElement body = document.getBody();

    private List<SpanElement> errorSpanContainersList = new ArrayList<>();

    private List<Element> formSectionContainers = generateFormLayoutContainers();

    private NameInputField nameInputField = new NameInputField(90, Style.Unit.PCT, 34, Style.Unit.PX);
    private JobSelectField jobSelectField = new JobSelectField(90, Style.Unit.PCT, 34, Style.Unit.PX);

    private DivElement formRootContainer;
    private InputElement agreementCheckBox;

    private ExampleAppResources resources;

    @Override
    public void onModuleLoad() {

        resources = GWT.create(ExampleAppResources.class);
        resources.css().ensureInjected();

        formRootContainer = document.createDivElement();
        formRootContainer.addClassName(ExampleAppResources.INSTANCE.css().form_root_container());

        body.appendChild(formRootContainer);

        for (Element element : formSectionContainers) {
            formRootContainer.appendChild(element);
        }

        final SpanElement nameSectionHeader = document.createSpanElement();
        nameSectionHeader.setInnerText(NAME_HEADER_LABEL);

        final SpanElement jobSectionHeader = document.createSpanElement();
        jobSectionHeader.setInnerText(JOB_HEADER_LABEL);

        final SpanElement agreementSectionHeader = document.createSpanElement();
        agreementSectionHeader.setInnerText(AGREE_TO_SEND_INFORMATION_LABEL);

        final ButtonElement sendButton = document.createPushButtonElement();
        sendButton.setInnerText(SEND_BUTTON_LABEL);
        sendButton.addClassName(ExampleAppResources.INSTANCE.css().send_button());

        agreementCheckBox = document.createCheckInputElement();

        formSectionContainers.get(2).appendChild(agreementCheckBox);
        formSectionContainers.get(2).appendChild(agreementSectionHeader);

        formSectionContainers.get(0).appendChild(nameSectionHeader);
        formSectionContainers.get(0).appendChild(nameInputField.get());

        formSectionContainers.get(1).appendChild(jobSectionHeader);
        formSectionContainers.get(1).appendChild(jobSelectField.get());

        formSectionContainers.get(3).appendChild(sendButton);

        jobSelectField.addJobs(JobsProvider.get());

        Event.sinkEvents(sendButton, Event.ONCLICK);
        Event.setEventListener(sendButton, event -> {
            if (Event.ONCLICK == event.getTypeInt()) {
                validateForm();
            }
        });
    }

    private List<Element> generateFormLayoutContainers() {

        List<Element> containers = new ArrayList<>();

        for (int i = 0; i < FORM_SECTIONS_COUNT; i++) {
            final DivElement element = document.createDivElement();
            element.addClassName(ExampleAppResources.INSTANCE.css().form_section());
            containers.add(element);
        }

        containers.get(0).setId("form_name_section");
        containers.get(1).setId("form_job_section");
        containers.get(2).setId("form_agreement_section");
        containers.get(3).setId("form_send_button_section");

        return containers;
    }

    private void validateForm() {

        List<String> errorMessages = new ArrayList<>();

        if (nameInputField != null && "".equals(nameInputField.getNameValue())) {
            errorMessages.add(PLEASE_FILL_NAME_FIELD_LABEL);
            nameInputField.highlight(false);
        } else {
            nameInputField.highlight(true);
        }

        if (jobSelectField != null && jobSelectField.getJobSelectedIndex() == -1) {
            errorMessages.add(PLEASE_CHOOSE_JOB_LABEL);
            jobSelectField.highlight(false);
        } else {
            jobSelectField.highlight(true);
        }

        if (agreementCheckBox != null && body.isOrHasChild(agreementCheckBox)) {
            if (!agreementCheckBox.isChecked()) {
                errorMessages.add(PLEASE_ACCEPT_AGR_LABEL);
            }
        }

        clearErrorMessages();

        if (!errorMessages.isEmpty()) {
            displayErrorMessages(formRootContainer, errorMessages);
        }
    }

    private void displayErrorMessages(final Element container, final List<String> errorMessagesList) {
        if (errorMessagesList != null && container != null) {
            for (String errorMessage : errorMessagesList) {
                final SpanElement errorSpanContainer = document.createSpanElement();
                errorSpanContainer.addClassName(ExampleAppResources.INSTANCE.css().error_message());
                errorSpanContainer.setInnerText(errorMessage);
                errorSpanContainersList.add(errorSpanContainer);
                container.appendChild(errorSpanContainer);
            }
        }
    }

    private void clearErrorMessages() {
        if (errorSpanContainersList != null) {
            for (SpanElement spanElement : errorSpanContainersList) {
                if (body.isOrHasChild(spanElement)) {
                    spanElement.removeFromParent();
                }
            }
        }
    }
}
