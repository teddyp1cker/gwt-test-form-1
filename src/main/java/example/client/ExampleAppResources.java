package example.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface ExampleAppResources extends ClientBundle {

    public static final ExampleAppResources INSTANCE = GWT.create(ExampleAppResources.class);

    @Source("static/images/login-icon.png")
    ImageResource loginIconImage();

    @Source("static/images/password-icon.png")
    ImageResource passwordIconImage();

    @Source("static/style.css")
    public ExampleAppStyle css();
}