package example.client;

import com.google.gwt.resources.client.CssResource;

public interface ExampleAppStyle extends CssResource {

    @ClassName("error_message")
    String error_message();

    @ClassName("form_section")
    String form_section();

    @ClassName("form_root_container")
    String form_root_container();

    @ClassName("job_choose_field")
    String job_choose_field();

    @ClassName("name_field")
    String name_field();

    @ClassName("send-button")
    String send_button();
}
