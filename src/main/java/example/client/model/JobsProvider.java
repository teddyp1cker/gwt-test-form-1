package example.client.model;

import java.util.Arrays;
import java.util.List;

public class JobsProvider {

    public static List<String> get() {
        String[] jobsArray = new String[]{"Tinker", "Taylor", "Soldier", "Sailor"};
        final List<String> jobs = Arrays.asList(jobsArray);
        return jobs;
    }
}
