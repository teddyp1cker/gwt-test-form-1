# Test GWT form
## How to build

Run gradle task using bundled gradle-wrapper :

```
./gradlew generateStaticIndexPage
```

It will compile all necessary GWT files and copy static html page with style to

```
build/gwt/out/example.Example
```

folder. Then you can check the result by open `index.html` file in any browser.
